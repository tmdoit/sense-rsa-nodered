module.exports = function(RED) 
{
    function Encrypt(config) 
    {  
        RED.nodes.createNode(this, config);
        var node = this;

        node.on('input', function(msg) 
        {
            const {generateKeyPair} = require('crypto');
            generateKeyPair('rsa', 
            {
                modulusLength: parseInt(config.bitlength),
                publicKeyEncoding:
                {
                    type: "spki",
                    format: "pem"
                },
                privateKeyEncoding: 
                {
                    type: 'pkcs8',
                    format: 'pem',
                    cipher: 'aes-256-cbc',
                    passphrase: ''
                }
            }, function(err, publicKey, privateKey)
            {
                msg.payload = {privateKey: privateKey, publicKey: publicKey};
                msg.topic = 'keypair';
                node.send(msg);
            });
        });
    }

    RED.nodes.registerType("rsa-generate-keypair", Encrypt);
}