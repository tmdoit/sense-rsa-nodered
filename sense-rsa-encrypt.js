module.exports = function(RED) 
{
    function Encrypt(config) 
    {
        const crypto = require('crypto');
            
        RED.nodes.createNode(this, config);
        var node = this;

        node.on('input', function(msg) 
        {
			var replaced = config.publickey.replace("-----BEGIN PUBLIC KEY-----", "")
			replaced = replaced.replace("-----END PUBLIC KEY-----", "")
			
			replaced = "-----BEGIN PUBLIC KEY-----\n" + replaced + "\n-----END PUBLIC KEY-----";
			
			if(this.context().publicKey === "")
				this.context().publicKey = replaced;
				
            let keyData = this.context().publicKey || replaced;

            if(msg.topic === "public-key")
            {
                this.context().publicKey = msg.payload;
            }
            else if(msg.topic === "keypair")
            {
                this.context().publicKey = msg.payload.publicKey;
            }
            else
            {
                if(typeof msg.payload == "object")
                {
                    msg.payload = JSON.stringify(msg.payload);
                }

                let buffer = Buffer.from(msg.payload, 'utf8')
                let encrypted = crypto.publicEncrypt(keyData, buffer);

                msg.payload = encrypted.toString('base64');
                node.send(msg);
            }
        });
    }

    RED.nodes.registerType("rsa-encrypt", Encrypt);
}